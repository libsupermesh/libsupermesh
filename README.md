## libsupermesh ##

libsupermesh parallel supermeshing library.

Development of libsupermesh has moved to: https://github.com/firedrakeproject/libsupermesh
